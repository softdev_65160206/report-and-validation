/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wittaya5329
 */
public class ArtistReport {
    private int id;
    private String name;
    private int totalqty;
    private float totalPrice;

    public ArtistReport(int id, String artistName, int toatlqty, float totalPrice) {
        this.id = id;
        this.name = artistName;
        this.totalqty = toatlqty;
        this.totalPrice = totalPrice;
    }
    
    public ArtistReport(String name, int toatlqty, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalqty = toatlqty;
        this.totalPrice = totalPrice;
    }
    public ArtistReport() {
        this(-1,"",0,0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalqty() {
        return totalqty;
    }

    public void setTotalqty(int toatlqty) {
        this.totalqty = toatlqty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ArtistReport{" + "id=" + id + ", name=" + name + ", toatlqty=" + totalqty + ", totalPrice=" + totalPrice + '}';
    }
    
     public static ArtistReport fromRS(ResultSet rs) {
        ArtistReport obj = new ArtistReport();
        try {
            obj.setId(rs.getInt("ArtistId"));
            obj.setName(rs.getString("Name"));
            obj.setTotalqty(rs.getInt("TotalQuantity"));
            obj.setTotalPrice(rs.getFloat("TotalPrice"));
        } catch (SQLException ex) {
            Logger.getLogger(ArtistReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
